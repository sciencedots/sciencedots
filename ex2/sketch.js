let sci;

let api = 'https://doaj.org/api/v1/search/articles/bibjson.author.name%3A%22';
let apiKey = '"';

let author_list = [];
let i = 0;
let j = 0;
let input;
let input2;
let encontrou = false;

function setup() {
  let button = select('#submit');
  button.mousePressed(sciAsk);
  input = select('#search');
  input2 = select('#search2');
}

function sciAsk() {
  let url;

  if (input == input2.value()) {
    createElement('h2', "FUNCIONA!");
    encontrou = true;
  }

  else if (j==0) {
    url = api + input.value() + apiKey;
    j += 1;  
  }
  else {
    url = api + input + apiKey;
  }
  loadJSON(url, gotData);
}

function gotData(data) {
  sci = data;
  for (let item of sci.results) {
    if (encontrou)
      return;
    for (let author of item.bibjson.author) {
      if (author_list.includes(author.name) == false) {
        author_list.push(author.name);
        createElement('h3', author.name);
      }
      
      if (input != author_list[i]) {
        input = author_list[i];
        sciAsk(author_list[i]);
        
      }
      i += 1;
    }
  }
}
