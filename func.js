//function randomExample() {
    //var exemplos = ['Machine Learning', 'Yoshiharu Kohayakawa', 'Nina Sumiko Tomita Hirata', 'Medicina', 'Inteligência Artifical', 'Programação paralela'];
    //var r = exemplos[parseInt(Math.random()*exemplos.length)];
    //return r;
//}

//document.querySelector("#input1").setAttribute('placeholder', randomExample());
//document.querySelector("#input2").setAttribute('placeholder', randomExample());
document.querySelector("#input1").value = 'Nina S. T. Hirata';
document.querySelector("#input2").value = 'Psychiatry';

function dropdown(){
  var dropdown = document.querySelector('.dropdown');
  dropdown.addEventListener('click', function(event) {
    event.stopPropagation();
    dropdown.classList.toggle('is-active');
  });
}

var d1 = document.querySelector('.d1');
d1.addEventListener('click', function(event) {
  event.stopPropagation();
  d1.classList.toggle('is-active');
  document.querySelector('.d2').classList.remove('is-active')
  document.querySelector('.d3').classList.remove('is-active')
});

var d2 = document.querySelector('.d2');
d2.addEventListener('click', function(event) {
  event.stopPropagation();
  d2.classList.toggle('is-active');
  document.querySelector('.d1').classList.remove('is-active')
  document.querySelector('.d3').classList.remove('is-active')
});

var _d3 = document.querySelector('.d3');
_d3.addEventListener('click', function(event) {
  event.stopPropagation();
  _d3.classList.toggle('is-active');
  document.querySelector('.d1').classList.remove('is-active')
  document.querySelector('.d2').classList.remove('is-active')
});

var button = document.querySelector('.fields1');
button.addEventListener('click', function(event) {
    event.stopPropagation();   
    this.classList.toggle('is-info');
    this.classList.toggle('is-active');
    document.querySelector('.papers1').classList.remove('is-info');
    document.querySelector('.papers1').classList.remove('is-active');
    document.querySelector('.researchers1').classList.remove('is-info')
    document.querySelector('.researchers1').classList.remove('is-active');
});

var button = document.querySelector('.papers1');
button.addEventListener('click', function(event) {
    event.stopPropagation();   
    this.classList.toggle('is-info');
    this.classList.toggle('is-active');
    document.querySelector('.fields1').classList.remove('is-info');
    document.querySelector('.fields1').classList.remove('is-active');
    document.querySelector('.researchers1').classList.remove('is-info')
    document.querySelector('.researchers1').classList.remove('is-active');
});

var button = document.querySelector('.researchers1');
button.addEventListener('click', function(event) {
    event.stopPropagation();   
    this.classList.toggle('is-info');
    this.classList.toggle('is-active');
    document.querySelector('.fields1').classList.remove('is-info');
    document.querySelector('.fields1').classList.remove('is-active');
    document.querySelector('.papers1').classList.remove('is-info')
    document.querySelector('.papers1').classList.remove('is-active');
});

var button = document.querySelector('.fields2');
button.addEventListener('click', function(event) {
    event.stopPropagation();   
    this.classList.toggle('is-info');
    this.classList.toggle('is-active');
    document.querySelector('.papers2').classList.remove('is-info');
    document.querySelector('.papers2').classList.remove('is-active');
    document.querySelector('.researchers2').classList.remove('is-info')
    document.querySelector('.researchers2').classList.remove('is-active');
});

var button = document.querySelector('.papers2');
button.addEventListener('click', function(event) {
    event.stopPropagation();   
    this.classList.toggle('is-info');
    this.classList.toggle('is-active');
    document.querySelector('.fields2').classList.remove('is-info');
    document.querySelector('.fields2').classList.remove('is-active');
    document.querySelector('.researchers2').classList.remove('is-info')
    document.querySelector('.researchers2').classList.remove('is-active');
});

var button = document.querySelector('.researchers2');
button.addEventListener('click', function(event) {
    event.stopPropagation();   
    this.classList.toggle('is-info');
    this.classList.toggle('is-active');
    document.querySelector('.fields2').classList.remove('is-info');
    document.querySelector('.fields2').classList.remove('is-active');
    document.querySelector('.papers2').classList.remove('is-info')
    document.querySelector('.papers2').classList.remove('is-active');
});
