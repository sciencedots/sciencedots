var config = {
    dataSource: 'graph.json',
    forceLocked: false,
    backgroundColour: "#dbdbdb",
    edgeStyle: {
      "all": {
        "width": 4,
        "color": "#000",
        "opacity": 0.2,
        "selected": {
            "opacity": 1
          },
        "highlighted": {
            "opacity": 1
          },
        "hidden": {
            "opacity": 0
          }
      }
    },
    nodeStyle: {
      "all": {
          "radius": 10,
          "color"  : "#68B9FE",
          "borderColor": "#000",
          "captionColor": "#000",
          "captionBackground": null,
          "captionSize": 12,
          "selected": {
              "color" : "#FFFFFF",
              "borderColor": "#349FE3"
          },
          "highlighted": {
              "color" : "#EEEEFF"
          },
          "hidden": {
              "color": "none",
              "borderColor": "none"
          }
      }
    },
    nodeTypes: {"node_type":[ "Maintainer",
                              "Contributor"]},
    nodeCaption: function(node){
      return node.caption + " " + node.fun_fact;}
};
alchemy = new Alchemy(config);


document.querySelector('#search').onclick = function() {
    let p1 = document.querySelector('#input1').value;
    let p2 = document.querySelector('#input2').value;

    fetch('http://127.0.0.1:8000/pesquisadores/' + p1 + '/' + p2).then(function(data) {
        return data.json();
    }).then(function(data) {
        config.dataSource = data;
        alchemy = new Alchemy(config);
    });
};

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
